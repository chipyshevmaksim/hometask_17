﻿#include <iostream>
using namespace std;

//class vector
//{
//private:
//	int a;
//	
//public:
//	int GetA()
//	{
//		return a;
//	}
//	void SetA(int newA)
//	{
//		a = newA;
//	}
//};
//
//
//int main()
//{
//	vector example;
//	example.SetA(5000);
//	std::cout<<example.GetA();
//}

class MyClass
{
public:
	MyClass(double _Z, double _Y, double _X) : z(_Z), y(_Y), x(_X)
	{}

	void show()
	{
		std::cout << z << ' ' << y << ' ' << x;
	}

	void modul()
	{
		double a = sqrt(z * z + y * y + x * x);
		std::cout << a;
	}

private:
	double z = 0;
	double y = 0;
	double x = 0;
};

int main()
{
	MyClass l(200, 10, 150);
	l.modul();
}